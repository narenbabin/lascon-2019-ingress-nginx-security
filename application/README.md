# LASCON 2019 - Kubernetes Ingress-Nginx Security from Beginner to Expert Demos

This project contains sample yamls for configuring different security features within Ingress-NGINX.

## Contents

The **application** folder contains the deployment as well as the service needed for the application to run. Both must be applied before creating ingress rules.

The **security** folder contains the ingress resources to setup the main parts of the demo.
