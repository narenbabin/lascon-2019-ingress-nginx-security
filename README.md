# LASCON 2019 - Kubernetes Ingress-Nginx Security from Beginner to Expert Demos

This project contains sample yamls for configuring different security features within Ingress-NGINX.

## Contents

The **application** folder contains the deployment as well as the service needed for the application to run. Both must be applied before creating ingress rules.

The **security** folder contains the ingress resources to setup the main parts of the demo.

## Requirements

In order to follow the presentation, you will need access to a [Kubernetes Cluster](https://docs.gitlab.com/ee/user/project/clusters/index.html#adding-and-removing-clusters).
You can create a cluster and easily install ingress by following GitLab's [Auto DevOps guide](https://docs.gitlab.com/ee/topics/autodevops/).

## Usage

### Installing the Application

```bash
kubectl apply -f application/meow_deploy.yaml
deployment.extensions/meow created

kubectl apply -f application/meow_svc.yaml 
service/meow-svc created

kubectl get svc --all-namespaces | grep ingress-nginx-ingress-controller
gitlab-managed-apps             ingress-nginx-ingress-controller         LoadBalancer   10.0.6.209    
35.226.75.136   80:31677/TCP,443:30385/TCP   8d

export LB_IP=35.226.75.136
```

### Basic Auth

```bash
htpasswd -cb auth us3r p4ssw0rd
Adding password for user us3r

kubectl create secret generic basic-auth --from-file=auth
secret/basic-auth created

kubectl apply -f security/basic_auth/basic_auth.yaml
ingress.extensions/meow-ingress created

curl -H "Host: meow.com" $LB_IP
<html>
<head><title>401 Authorization Required</title></head>
<body bgcolor="white">
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.13.8</center>
</body>
</html>

curl -H "Host: meow.com" $LB_IP -u us3r:p4ssw0rd
Hostname: meow-d4759bbcb-kblqc
Pod Information:
        -no pod information available-
Server values:
        server_version=nginx: 1.12.2 - lua: 10010
Request Information:
        client_address=10.48.1.7
        method=GET
        real path=/
        query=
        request_version=1.1
        request_scheme=http
        request_uri=http://meow.com:8080/
Request Headers:
        accept=*/*
        connection=close
        host=meow.com
        user-agent=curl/7.64.1
        x-forwarded-for=10.128.0.11
        x-forwarded-host=meow.com
        x-forwarded-port=80
        x-forwarded-proto=http
        x-original-uri=/
        x-real-ip=10.128.0.11
        x-scheme=http
Request Body:
        -no body in request-
```

### External Auth

```bash
kubectl apply -f security/external_auth/external_auth.yaml
ingress.extensions/meow-ingress created

curl -H "Host: meow.com" $LB_IP
<html>
<head><title>401 Authorization Required</title></head>
<body bgcolor="white">
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.13.8</center>
</body>
</html>

curl -H "Host: meow.com" $LB_IP -u user:passwd
Hostname: meow-d4759bbcb-kblqc
Pod Information:
        -no pod information available-
Server values:
        server_version=nginx: 1.12.2 - lua: 10010
Request Information:
        client_address=10.48.1.7
        method=GET
        real path=/
        query=
        request_version=1.1
        request_scheme=http
        request_uri=http://meow.com:8080/
Request Headers:
        accept=*/*
        connection=close
        host=meow.com
        user-agent=curl/7.64.1
        x-forwarded-for=10.128.0.11
        x-forwarded-host=meow.com
        x-forwarded-port=80
        x-forwarded-proto=http
        x-original-uri=/
        x-real-ip=10.128.0.11
        x-scheme=http
Request Body:
        -no body in request-
```

### Mutual Auth

```bash
sh security/mutual_auth/generate_certs.sh
Generating a 4096 bit RSA private key
......++
.....................................................++
writing new private key to 'ca.key'
-----
Generating a 4096 bit RSA private key
......................................................................................................................................................................................++
......................................................++
writing new private key to 'server.key'
-----
Signature ok
subject=/CN=meow.com
Getting CA Private Key
Generating a 4096 bit RSA private key
.....................................................................................................................................................................................++
.......................................++
writing new private key to 'client.key'
-----
Signature ok
subject=/CN=Fern
Getting CA Private Key
secret/my-certs created

kubectl create secret generic my-certs --from-file=tls.crt=server.crt --from-file=tls.key=server.key --from-file=ca.crt=ca.crt
secret/my-certs created

kubectl apply -f security/mutual_auth/mutual_auth.yaml
ingress.extensions/meow-ingress configured

sudo -- sh -c "echo $LB_IP meow.com >> /etc/hosts"

curl -H https://meow.com -k
<html>
<head><title>400 No required SSL certificate was sent</title></head>
<body bgcolor="white">
<center><h1>400 Bad Request</h1></center>
<center>No required SSL certificate was sent</center>
<hr><center>nginx/1.13.8</center>
</body>
</html>

curl https://meow.com/ -k --cert client.crt --key client.key                          
Hostname: meow-d4759bbcb-kblqc
Pod Information:
        -no pod information available-
Server values:
        server_version=nginx: 1.12.2 - lua: 10010
Request Information:
        client_address=10.48.1.7
        method=GET
        real path=/
        query=
        request_version=1.1
        request_scheme=http
        request_uri=http://meow.com:8080/
Request Headers:
        accept=*/*
        connection=close
        host=meow.com
        ssl-client-dn=CN=Fern
        ssl-client-verify=SUCCESS
        user-agent=curl/7.64.1
        x-forwarded-for=10.48.1.1
        x-forwarded-host=meow.com
        x-forwarded-port=443
        x-forwarded-proto=https
        x-original-uri=/
        x-real-ip=10.48.1.1
        x-scheme=https
Request Body:
        -no body in request-
```

### ModSecurity

```bash
curl -H "Host: meow.com" $LB_IP -k -H "user-agent: absinthe"
Hostname: meow-d4759bbcb-kblqc
Pod Information:
        -no pod information available-
Server values:
        server_version=nginx: 1.12.2 - lua: 10010
Request Information:
        client_address=10.48.1.7
        method=GET
        real path=/
        query=
        request_version=1.1
        request_scheme=http
        request_uri=http://meow.com:8080/
Request Headers:
        accept=*/*
        connection=close
        host=meow.com
        user-agent=absinthe
        x-forwarded-for=10.48.1.1
        x-forwarded-host=meow.com
        x-forwarded-port=80
        x-forwarded-proto=http
        x-original-uri=/
        x-real-ip=10.48.1.1
        x-scheme=http
Request Body:
        -no body in request-

kubectl -n gitlab-managed-apps exec -it $(kubectl get pods -n gitlab-managed-apps | grep 'ingress-controller' | awk '{print $1}') -- tail /var/log/modsec_audit.log
ModSecurity: Warning. Matched "Operator `PmFromFile' with parameter `scanners-user-agents.data' against variable `REQUEST_HEADERS:User-Agent' (Value: `absinthe' ) [file "/etc/nginx/owasp-modsecurity-crs/rules/REQUEST-913-SCANNER-DETECTION.conf"] [line "17"] [id "913100"] [rev "2"] [msg "Found User-Agent associated with security scanner"] [data "Matched Data: absinthe found within REQUEST_HEADERS:User-Agent: absinthe"] [severity "2"] [ver "OWASP_CRS/3.0.0"] [maturity "9"] [accuracy "9"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-reputation-scanner"] [tag "OWASP_CRS/AUTOMATION/SECURITY_SCANNER"] [tag "WASCTC/WASC-21"] [tag "OWASP_TOP_10/A7"] [tag "PCI/6.5.10"] [hostname "10.48.1.1"] [uri "/"] [unique_id "157189024423.708128"] [ref "o0,8v54,8t:lowercase"]
ModSecurity: Warning. Matched "Operator `Ge' with parameter `%{tx.inbound_anomaly_score_threshold}' against variable `TX:ANOMALY_SCORE' (Value: `5' ) [file "/etc/nginx/owasp-modsecurity-crs/rules/REQUEST-949-BLOCKING-EVALUATION.conf"] [line "36"] [id "949110"] [rev ""] [msg "Inbound Anomaly Score Exceeded (Total Score: 5)"] [data ""] [severity "2"] [ver ""] [maturity "0"] [accuracy "0"] [tag "application-multi"] [tag "language-multi"] [tag "platform-multi"] [tag "attack-generic"] [hostname "10.48.1.1"] [uri "/"] [unique_id "157189024423.708128"] [ref ""]
ModSecurity: Warning. Matched "Operator `Ge' with parameter `%{tx.inbound_anomaly_score_threshold}' against variable `TX:INBOUND_ANOMALY_SCORE' (Value: `5' ) [file "/etc/nginx/owasp-modsecurity-crs/rules/RESPONSE-980-CORRELATION.conf"] [line "61"] [id "980130"] [rev ""] [msg "Inbound Anomaly Score Exceeded (Total Inbound Score: 5 - SQLI=0,XSS=0,RFI=0,LFI=0,RCE=0,PHPI=0,HTTP=0,SESS=0): Found User-Agent associated with security scanner"] [data ""] [severity "0"] [ver ""] [maturity "0"] [accuracy "0"] [tag "event-correlation"] [hostname "10.48.1.1"] [uri "/"] [unique_id "157189024423.708128"] [ref ""]
```

